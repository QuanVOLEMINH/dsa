"""
    https://leetcode.com/problems/find-minimum-in-rotated-sorted-array-ii/
"""

from typing import List
import pytest


class Solution:
    """
    Solution
    """

    def find_min(self, nums: List[int]) -> int:
        """
        Similar to 153, except that when mid = right, we can have:
                3 3 3 1 3, search right part
                1 3 3 3 3, search left part
                3 1 3 3 3, search left part
                3 3 3 1 3, search right part

        Hence we could not search for only one part => need to reduce right by 1
            => in the worst case, when all elements are the same, the time complexity is O(n)
        """
        if len(nums) == 0:
            return - 1

        left_idx = 0
        right_idx = len(nums) - 1

        while left_idx < right_idx:
            mid_idx = left_idx + (right_idx-left_idx)//2

            if nums[mid_idx] < nums[right_idx]:
                right_idx = mid_idx
            elif nums[mid_idx] > nums[right_idx]:
                left_idx = mid_idx + 1
            else:
                right_idx -= 1  # hardest thought

        return nums[left_idx]


test_data = [
    ([1, 3, 5], 1),
    ([2, 2, 0, 1], 0),
    ([3, 3, 3, 3], 3),
    ([1, 1], 1),
    ([], -1),
    ([3, 1], 1),
    ([3, 1, 3, 3, 3], 1)
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()
    assert solution.find_min(arg1) == arg2
