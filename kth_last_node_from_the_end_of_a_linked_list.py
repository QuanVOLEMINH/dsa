"""
https://www.geeksforgeeks.org/nth-node-from-the-end-of-a-linked-list/
"""
import pytest
from q_data_structures import QLNode, QLinkedList


class Solution:
    """
    Solution goes here
    """

    def solve(self, arg1, arg2):
        """
        For each node, call search function to the next node
        Increase the count from the end
        If count == position => set the result
        """

        def search(arg1, arg2, count):
            if arg1 is None:
                return

            search(arg1.next, arg2, count)
            count[0] += 1
            if count[0] == arg2:
                count[1] = arg1.val
                return
        count = [0, -1]
        search(arg1.head, arg2, count)

        return count[1]


# quality over quantity when it comes to test cases
head1 = QLNode('A').set_next(
    QLNode('B').set_next(QLNode('C').set_next(QLNode('D'))))
linked_list_1 = QLinkedList(head1)

test_data = [
    (linked_list_1, 3, 'B'),
    (linked_list_1, 2, 'C'),
    (linked_list_1, 1, 'D'),
    (linked_list_1, 5, -1)
]


@ pytest.mark.parametrize('arg1, arg2, arg3', test_data)
def test(arg1, arg2, arg3):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.solve(arg1, arg2) == arg3
