"""
Problem link
"""

import pytest


class Solution:
    """
    Solution goes here
    """

    def solve(self, arg1) -> [str]:
        """
        Swap left and right until left == right
        """
        nums = list(arg1)
        left, right = 0, len(nums)-1
        while left < right:
            nums[left], nums[right] = nums[right], nums[left]
            left += 1
            right -= 1
        return nums


# quality over quantity when it comes to test cases
test_data = [
    ([2, 3, 4, 5], [5, 4, 3, 2])
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.solve(arg1) == arg2
