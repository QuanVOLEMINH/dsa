from typing import List
import pytest


class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        def search(permutation, nums, n):
            if len(permutation) == n:
                result.append(list(permutation))
            else:
                for i, val in enumerate(nums):
                    if chosen[i]:
                        continue
                    permutation.append(val)
                    chosen[i] = True
                    search(permutation, nums, n)
                    permutation.pop()
                    chosen[i] = False
        result = []
        n = len(nums)
        chosen = [False] * n

        search([], nums, n)

        return result


test_data = [
    ([1, 2, 3], [[1, 2, 3],
                 [1, 3, 2],
                 [2, 1, 3],
                 [2, 3, 1],
                 [3, 1, 2],
                 [3, 2, 1]]),
    ([0, 1], [[0, 1],
              [1, 0]]),
    ([1], [[1]])
]


@pytest.mark.parametrize('arg1,arg2', test_data)
def test(arg1, arg2):
    solution = Solution()
    assert solution.permute(arg1) == arg2
