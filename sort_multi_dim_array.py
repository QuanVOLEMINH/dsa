
arr = [[12, 9], [1, 4], [15, 15], [3, 3242], [3, 123], [12, 14]]

first_asc_second_asc = sorted(arr, key=lambda item: (item[0], item[1]))
first_asc_second_desc = sorted(arr, key=lambda item: (item[0], -item[1]))

print(first_asc_second_asc)
print(first_asc_second_desc)
