import pytest


def rotate_string(s: str, d: int, direction: str) -> str:

    if direction == 'left':
        return s[d:] + s[:d]

    return s[len(s)-d:] + s[:len(s)-d]


test_data = [
    ('dasdfhdlkfds', 2, 'left', 'sdfhdlkfdsda'),
    ('fsdfdsfdl', 3, 'right', 'fdlfsdfds')
]


@pytest.mark.parametrize('arg1, arg2, arg3, arg4', test_data)
def test1(arg1, arg2, arg3, arg4):
    assert rotate_string(arg1, arg2, arg3) == arg4


pytest.main()
