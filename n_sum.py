"""
n sum
"""

from typing import List
from collections import Counter
import pytest


class Solution:
    """
    Solution goes here
    """

    def solve_fix_count(self, candidates, target, combi_count=3, with_dup=True) -> List[List[int]]:
        """
        Explain briefly the algorithm
        """

        result = []
        combi = []

        def search(nums, total):
            if total < 0:
                return

            if total == 0 and len(combi) == combi_count:
                result.append(list(combi))
                return

            for idx, num in enumerate(nums):
                combi.append(num)
                start_idx = idx if with_dup else idx+1
                search(nums[start_idx:], total - num)
                combi.pop()

        search(candidates, target)

        return result

    def solve_no_fix_count(self, candidates, target, with_dup=True) -> List[List[int]]:
        """
        Explain briefly the algorithm
        """

        result = []
        result_dict = []
        combi = []

        def search(nums, total):
            if total < 0:
                return

            if total == 0 and Counter(combi) not in result_dict:
                result_dict.append(Counter(combi))
                result.append(list(combi))
                return

            for idx, num in enumerate(nums):
                combi.append(num)
                start_idx = idx if with_dup else idx+1
                search(nums[start_idx:], total - num)
                combi.pop()

        search(candidates, target)

        return result


test_data = [
    ([2, 3, 6, 7], 7, 3, True, [[2, 2, 3]]),
    ([2, 3, 5], 8, 3, True, [[2, 3, 3]]),
    ([1], 2, 3, True, [])
]

# test_data_2 = [
#    ([10, 1, 2, 7, 6, 1, 5], 8, False, [[1, 1, 6], [1, 2, 5], [1, 7], [2, 6]])
# ]


@ pytest.mark.parametrize('arg1, arg2, arg3,arg4,arg5', test_data)
def test_n_sum(arg1, arg2, arg3, arg4, arg5):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.solve_fix_count(arg1, arg2, arg3, arg4) == arg5


# @ pytest.mark.parametrize('arg1, arg2, arg3,arg4', test_data_2)
# def test_n_sum_no_fix_count(arg1, arg2, arg3, arg4):
#    """
#    Tests go here
#    """
#    solution = Solution()
#
#    assert solution.solve_no_fix_count(arg1, arg2, arg3) == arg4
