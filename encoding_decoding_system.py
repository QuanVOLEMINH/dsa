def encode(input_str, key) -> str:
    ks = str(key)
    kl = len(ks)
    result = ''
    for i, c in enumerate(input_str):
        result += c*(i % kl+1)
    return result


def decode(ds, key):
    ks = str(key)
    kl = len(ks)
    result = ''

    idx = 0
    i = 0

    while i < len(ds):
        jump = int(ks[idx % kl])
        result += ds[i]
        i += jump
        idx += 1

    return result


print(encode("Open", 123))
print(decode("Oppeeen", 123))
print(encode("BiiVaaQiu", 1234))
print(decode(encode("BiiVaaQiu", 1234), 1234))
