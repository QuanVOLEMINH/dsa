"""
Lowest common ancestor
"""

from q_data_structures import QTreeNode


class Solution:
    """
    Solution
    """

    def lowestCommonAncestor(self, root: QTreeNode, p: QTreeNode, q: QTreeNode) -> QTreeNode:
        """
        Find the lowest common ancestor
        """
        def recurse_tree(cur_node):
            if not cur_node:
                return False

            left = recurse_tree(cur_node.left)
            right = recurse_tree(cur_node.right)

            mid = cur_node == p or cur_node == q

            if mid+left+right == 2:
                self.ans = cur_node

            return mid or left or right

        recurse_tree(root)

        return self.ans
