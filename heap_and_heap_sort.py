"""
Test heap and heap sort.
"""
import pytest
from q_data_structures import QMaxHeap


def heap_sort(max_heap: QMaxHeap):
    """
    heap sort
    after n iterations the heap is empty
    each iteration involves a call to extract_max, that means it involves a swap
    and a max_heapify operation, hence it takes O(logn) time.
    Overall: O(nlogn)
    """
    sorted_arr = []
    while max_heap.size:
        sorted_arr.append(max_heap.extract_max())

    return sorted_arr


test_data = [
    ([2, 3, 4, 1, 5], [5, 4, 3, 2, 1]),
    ([4, 1, 3, 2, 16, 9, 10, 14, 8, 7], [16, 14, 10, 9, 8, 7, 4, 3, 2, 1])
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Tests go here
    """
    max_heap = QMaxHeap(list(arg1))
    assert heap_sort(max_heap) == arg2
