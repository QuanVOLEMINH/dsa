"""
Print a binary tree vertically => print column by column, from top to bottom
"""

from q_data_structures import QTreeNode


def get_vertical_order(root_node: QTreeNode, hd: int, d: dict) -> None:
    """
    Get vertical order (horizontal distance) of all nodes to a dictionary
    """
    if not root_node:
        return
    if hd not in d:
        d[hd] = [root_node.key]
    else:
        d[hd].append(root_node.key)

    get_vertical_order(root_node.left, hd+1, d)
    get_vertical_order(root_node.right, hd-1, d)


def print_vertical_order(root_node: QTreeNode) -> None:
    """
        store vertical order (Horizontal distance) in a dict
    """

    # get vertical order or we can use a Queue
    d = dict()
    hd = 0
    get_vertical_order(root_node, hd, d)

    for _, node_values in sorted(d.items(), reverse=True):
        print(*node_values)


root = QTreeNode(1)
root.left = QTreeNode(2)
root.right = QTreeNode(3)
root.left.left = QTreeNode(4)
root.left.right = QTreeNode(5)
root.right.left = QTreeNode(6)
root.right.right = QTreeNode(7)
root.right.left.right = QTreeNode(8)
root.right.right.right = QTreeNode(9)


print_vertical_order(root)
