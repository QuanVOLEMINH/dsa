"""
https://leetcode.com/problems/permutation-in-string/
"""

import pytest


class Solution:
    """
    Solution goes here
    """

    def solve(self, str1, str2) -> bool:
        """
        Sliding window + count array
        """
        if str1 is None or str2 is None:
            return False

        N1, N2 = len(str1), len(str2)
        if N1 > N2:
            return False

        count1, count2 = [0]*26, [0]*26
        for i in range(N1):
            count1[self.idx(str1[i])] += 1
            count2[self.idx(str2[i])] += 1

        for i in range(N2 - N1):
            if count1 == count2:
                return True
            count2[self.idx(str2[i+N1])] += 1
            count2[self.idx(str2[i])] -= 1

        return count1 == count2

    def idx(self, ch) -> int:
        return ord(ch) - ord('a')


# quality over quantity when it comes to test cases
test_data = [
    (["ab", "eidbaoooo"], True),
    (["ab", "eidboaooo"], False),
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.solve(arg1[0], arg1[1]) == arg2
