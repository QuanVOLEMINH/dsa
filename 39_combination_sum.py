"""
https://leetcode.com/problems/combination-sum/
"""

from typing import List
import pytest


class Solution:
    """
    Solution goes here
    """

    def solve(self, candidates, target) -> List[List[int]]:
        """
        Explain briefly the algorithm
        """

        result = []
        combi = []

        def search(nums, total):
            if total < 0:
                return

            if total == 0:
                result.append(list(combi))
                return

            for idx, num in enumerate(nums):
                combi.append(num)
                search(nums[idx:], total - num)
                combi.pop()

        search(candidates, target)

        return result


test_data = [
    ([2, 3, 6, 7], 7, [[2, 2, 3], [7]]),
    ([2, 3, 5], 8, [[2, 2, 2, 2], [2, 3, 3], [3, 5]]),
    ([1], 2, [[1, 1]])
]


@pytest.mark.parametrize('arg1, arg2, arg3', test_data)
def test(arg1, arg2, arg3):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.solve(arg1, arg2) == arg3
