from q_data_structures import QBinaryTree, QTreeNode


a2 = QTreeNode(12).set_left(QTreeNode(14)).set_right(QTreeNode(15))
a3 = QTreeNode(20).set_left(QTreeNode(19)).set_right(QTreeNode(22))
a1 = QTreeNode(10).set_left(a2).set_right(a3)

tree = QBinaryTree(a1)
tree.print_root()


def second_largest_element(root: QTreeNode) -> int:

    def secondLargestUtil(root, c=[0]):

        if root == None or c[0] >= 2:
            return

        secondLargestUtil(root.right, c)

        c[0] += 1

        if c[0] == 2:
            x = root.key
            return

        secondLargestUtil(root.left, c)
    secondLargestUtil(root)


print('-----')
print(second_largest_element(tree.root))
