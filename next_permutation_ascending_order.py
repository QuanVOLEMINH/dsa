# from typing import List
# import py
# import pytest


# def next_permutation(nums: List[int]) -> List[int]:

#     n = len(nums)

#     def search(permutation, chosen):
#         if len(permutation) == n:
#             print(permutation)
#             return
#         for idx, num in enumerate(nums):
#             if chosen[idx] == True:
#                 continue
#             if len(permutation) == 0:
#                 permutation.append(num)
#                 chosen[idx] = True
#                 search(permutation, chosen)
#                 permutation.pop()
#                 chosen[idx] = False
#             else:
#                 last = permutation[0]
#                 if num < last:
#                     permutation = []
#                     chosen[idx-1] = False
#                     search(permutation, chosen)
#                 else:
#                     permutation.append(num)
#                     chosen[idx] = True
#                     search(permutation, chosen)
#                     permutation.pop()
#                     chosen[idx] = False

#     search([], [False]*n)


# test_data = [
#     ([3, 1, 2], [1, 2, 3]),
#     ([2, 1], [1, 2])
# ]


# @pytest.mark.parametrize('arg1, arg2', test_data)
# def test(arg1, arg2):
#     assert next_permutation(arg1) == arg2
