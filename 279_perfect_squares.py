"""
    https://leetcode.com/problems/perfect-squares/
"""
import pytest


class Solution:
    def numSquares(self, n: int) -> int:
        """
            recursion + dp + take min
        """
        def count_min(target, memo):
            if memo[target] != None:
                return memo[target]

            min_count = n

            cur_num = 1
            cur_sq = 1
            while cur_sq <= target:
                next_count = 1 + count_min(target-cur_sq, memo)
                min_count = min(min_count, next_count)
                cur_num += 1
                cur_sq = cur_num*cur_num

            memo[target] = min_count

            return memo[target]

        return count_min(n, [0] + [None]*n)


test_data = [(13, 2), (12, 3)]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    assert Solution().numSquares(arg1) == arg2
    assert Solution().numSquares(arg1) == arg2
