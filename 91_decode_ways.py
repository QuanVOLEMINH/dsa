"""
    https://leetcode.com/problems/decode-ways/
    https://www.youtube.com/watch?v=qli-JCrSwuk
"""
import py
import pytest

# From codedojo


def helper(data, k):
    if k == 0:
        return 1
    s = len(data) - k
    if data[s] == '0':
        return 0
    result = helper(data, k - 1)
    if k >= 2 and int(data[s:s+2]) <= 26:
        result += helper(data, k-2)

    return result


def num_ways(data: str) -> int:
    return helper(data, len(data))


def helper_dp(data, k, memo):
    if k == 0:
        return 1

    s = len(data) - k
    if data[s] == '0':
        return 0

    if memo[k] != None:
        return memo[k]

    result = helper_dp(data, k-1, memo)

    if k >= 2 and int(data[s:s+2]) <= 26:
        result += helper_dp(data, k-2, memo)

    memo[k] = result
    return result


def num_ways_dp(data: str) -> int:
    memo = [None] * (len(data) + 1)
    return helper_dp(data, len(data), memo)


class Solution:
    def numDecodings(self, s: str) -> int:
        def search(s, n, dp) -> int:
            print(dp)
            if dp[n] != None:
                return dp[n]

            if s[0] == '0':
                return 0

            result = search(s[1:], n-1, dp)

            if n >= 2 and int(s[:2]) <= 26:
                result += search(s[2:], n-2, dp)

            dp[n] = result
            return result

        dp = [None]*(len(s)+1)
        dp[0] = 1  # empty string

        return search(s, len(s), dp)


test_data = [
    ('11106', 2),
    ('12', 2),
    ('226', 3),
    ('0', 0),
    ('06', 0),
    ('', 1)
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test_num_ways(arg1, arg2):
    assert num_ways(arg1) == arg2


@pytest.mark.parametrize('arg1, arg2', test_data)
def test_num_ways_dp(arg1, arg2):
    assert num_ways_dp(arg1) == arg2


@pytest.mark.parametrize('arg1, arg2', test_data)
def test_solution(arg1, arg2):
    assert Solution().numDecodings(arg1) == arg2
