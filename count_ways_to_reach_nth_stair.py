
dp1 = dict()


def fib1(n: int) -> int:
    if n <= 1:
        return n

    if n in dp1:
        return dp1[n]

    f1 = fib1(n-1)
    dp1[n-1] = f1
    f2 = fib1(n-2)
    dp1[n-2] = f2

    return f1+f2


dp2 = dict()


def fib2(n: int) -> int:
    if n <= 2:
        return n
    if n == 3:
        return 4

    if n in dp2:
        return dp1[n]

    f1 = fib2(n-1)
    dp2[n-1] = f1
    f2 = fib2(n-2)
    dp2[n-2] = f2
    f3 = fib2(n-3)
    return f1 + f2 + f3


def count_ways1(num_stairs: int) -> int:
    return fib1(num_stairs+1)


def count_ways2(num_stairs: int) -> int:
    return fib2(num_stairs+1)


# O(m*n)
def count_ways_common(num_stairs: int, max_step: int) -> int:
    assert num_stairs >= 0
    if num_stairs <= 2:
        return num_stairs

    res = [0 for _ in range(num_stairs)]
    res[0], res[1] = 1, 1

    for i in range(2, num_stairs):
        # if steps is a set of steps -> for j in steps: if  j<=i: res[i]+=res[i-j]
        for j in range(1, max_step+1):
            if j <= i:
                res[i] += res[i-j]

    return res[num_stairs-1]


def count_ways_opt(num_stairs: int, max_step: int) -> int:
    """
        sliding window

        the idea is:
            f(n) = f(n-1) + f(n-2)
            => f(n-1) = f(n) - f(n-2)

        at n + 1:
            temp = f(n)
            temp -= f(n-2) # oldest element
            temp += f(n)  # temp = f(n) + f(n) - f(n-2) = f(n) + f(n-1) = f(n+1)
    """
    temp = 0
    res = [1]

    for i in range(1, num_stairs+1):  # O(n)
        oldest_idx = i - max_step - 1
        latest_idx = i - 1

        if oldest_idx >= 0:
            temp -= res[oldest_idx]
        temp += res[latest_idx]

        res.append(temp)

    return res[num_stairs]


def num_ways_bottom_up(n):
    if n == 0 or n == 1:
        return 1
    nums = [0]*(n+1)
    nums[0] = 1
    nums[1] = 1
    for i in range(2, n+1):  # O(n)
        nums[i] = nums[i-1] + nums[i-2]

    return nums[n]


def num_ways_x(n, m):
    if n == 0:
        return 1
    nums = [0]*(n+1)
    nums[0] = 1
    for i in range(1, n+1):
        total = 0
        for j in m:
            if j <= i:
                total += nums[i-j]
        nums[i] = total
    return nums[n]


# for i in range(10):
#     print(count_ways1(i))
# print("= = = = =")
# for i in range(10):
#     print(count_ways2(i))
# print("= = = = =")
# for i in range(2, 10):
#     print(count_ways_common(i, 2))
# print("= = = = =")
# print(count_ways_opt(10, 3))

# print('= = = = =')
# for i in range(10):
#     print(num_ways_bottom_up(i))


print('= = = = =')
for i in range(10):
    print(num_ways_x(i, {1, 2, 3}))
