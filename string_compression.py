import pytest
from typing import List


class Solution:
    def compress(self, chars: List[int]):
        count = 1
        idx = 0
        for i in range(1, len(chars) + 1):
            if i < len(chars) and chars[i] == chars[i-1]:
                count += 1
                continue

            chars[idx] = chars[i-1]
            idx += 1  # behind the considered str
            if count > 1:
                s = str(count)
                chars[idx: idx + len(s)] = list(s)
                idx += len(s)

            if i == len(chars) and idx < i:
                del chars[idx:i]  # remove trailing values
            count = 1

        print(chars)
        return idx


def test1():
    # breakpoint()
    x = ["a", "a", "b", "b", "c", "c", "c"]
    s = Solution()
    count = s.compress(x)
    assert count == 6


def test2():
    # breakpoint()
    x = ["a", "a", "b", "b"]
    s = Solution()
    count = s.compress(x)
    assert count == 4


def test3():
    # breakpoint()
    x = ["a", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"]
    s = Solution()
    count = s.compress(x)
    assert count == 4


def test4():
    # breakpoint()
    x = ["a", "b", "e", "e", "x"]
    s = Solution()
    count = s.compress(x)
    assert count == 5


def test5():
    # breakpoint()
    x = ["ab", "ab"]
    s = Solution()
    count = s.compress(x)
    assert count == 2


def test6():
    # breakpoint()
    x = ["a", "a", "a", "b", "b", "a", "a"]
    s = Solution()
    count = s.compress(x)
    assert count == 6


pytest.main()
