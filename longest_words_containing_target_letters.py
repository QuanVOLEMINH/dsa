"""
GS coderpad round 1, question2
"""
import pytest
from typing import List


def longest_words_1(words: List[str], target_word: str) -> List[str]:
    """
        1. Find max_len and valid words
        2. Check that letters of each word are in target_word, using issubset built-in function
        3. O(m*n)
    """
    if len(words) == 0:
        return {}

    target_letters = set(target_word)  # O(m)
    candidates = set()
    max_len = 0
    for w in words:  # O(n)
        if set(w).issubset(target_letters) and len(w) >= max_len:  # O(k) + O(k)
            candidates.add(w)
            max_len = len(w)
    return {x for x in candidates if len(x) == max_len}


test_data = [
    ({}, 'abcz', {}),
    ({'toe', 'toes', 'toee', 'dogs', 'toessa'}, 'toegds', {'toes', 'toee', 'dogs'})
]


@pytest.mark.parametrize('arg1, arg2, arg3', test_data)
def test(arg1, arg2, arg3):
    assert longest_words_1(arg1, arg2) == arg3
