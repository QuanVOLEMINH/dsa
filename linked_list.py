"""
Problem link
"""

import copy
import pytest
from q_data_structures import QLNode, QLinkedList


class Solution:
    """
    Solution goes here
    """

    def print(self, arg1: QLinkedList) -> str:
        """
        Explain briefly the algorithm
        """
        return str(arg1)

    def insert_beginning(self, arg1: QLinkedList, arg2: QLNode) -> str:
        """
        Insert at the beginning
        """
        linked_list = copy.deepcopy(arg1)

        cur_node = linked_list.head

        if cur_node is None:
            cur_node = arg2
        else:
            arg2.next = cur_node
            linked_list.head = arg2

        return str(linked_list)

    def insert_end(self, arg1: QLinkedList, arg2: QLNode) -> str:
        """
        Insert at the end
        """
        linked_list = copy.deepcopy(arg1)

        cur_node = linked_list.head

        if cur_node is None:
            cur_node = arg2
        else:
            while cur_node.next is not None:
                cur_node = cur_node.next

            cur_node.next = arg2

        return str(linked_list)

    def reverse(self, arg1: QLinkedList) -> str:
        """
        Reverse a linked lint
        Use 3 pointers
        """
        linked_list = copy.deepcopy(arg1)

        p1 = linked_list.head
        p2 = None

        while p1.next is not None:
            p2 = p1.next
            p1.next = p2.next
            p2.next = linked_list.head
            linked_list.head = p2

        return str(linked_list)

    def delete_node(self, arg1: QLinkedList, arg2: QLNode) -> str:
        """
        Delete a node
        """
        linked_list = copy.deepcopy(arg1)

        cur_node = linked_list.head

        if cur_node is None:
            return str(linked_list)

        prev_node = None
        while cur_node is not None:

            if cur_node.val == arg2.val:
                if prev_node is None:
                    linked_list.head = cur_node.next
                else:
                    prev_node.next = cur_node.next
                break
            prev_node = cur_node
            cur_node = cur_node.next

        return str(linked_list)


# quality over quantity when it comes to test cases
head_1 = QLNode(5).set_next(QLNode(4).set_next(QLNode(7).set_next(QLNode(2))))
linked_list_1 = QLinkedList(head_1)

head_2 = QLNode(3).set_next(QLNode(1).set_next(
    QLNode(8).set_next(QLNode(9).set_next(QLNode(20)))))
linked_list_2 = QLinkedList(head_2)

test_data = [
    (linked_list_1, "[5 -> 4 -> 7 -> 2]")
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test_print(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.print(arg1) == arg2


test_data_2 = [
    (linked_list_1, QLNode(6), "[6 -> 5 -> 4 -> 7 -> 2]"),
    (linked_list_2, QLNode(11), "[11 -> 3 -> 1 -> 8 -> 9 -> 20]")
]


@pytest.mark.parametrize('arg1, arg2, arg3', test_data_2)
def test_insert_beginning(arg1, arg2, arg3):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.insert_beginning(arg1, arg2) == arg3


test_data_3 = [
    (linked_list_1, QLNode(6), "[5 -> 4 -> 7 -> 2 -> 6]"),
    (linked_list_2, QLNode(11), "[3 -> 1 -> 8 -> 9 -> 20 -> 11]")
]


@pytest.mark.parametrize('arg1, arg2, arg3', test_data_3)
def test_insert_end(arg1, arg2, arg3):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.insert_end(arg1, arg2) == arg3


test_data_4 = [
    (linked_list_1, "[2 -> 7 -> 4 -> 5]"),
    (linked_list_2, "[20 -> 9 -> 8 -> 1 -> 3]")
]


@pytest.mark.parametrize('arg1, arg2', test_data_4)
def test_reverse(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.reverse(arg1) == arg2


test_data_5 = [
    (linked_list_1, QLNode(4), "[5 -> 7 -> 2]"),
    (linked_list_2, QLNode(3), "[1 -> 8 -> 9 -> 20]"),
    (linked_list_1, QLNode(1), "[5 -> 4 -> 7 -> 2]")
]


@pytest.mark.parametrize('arg1, arg2, arg3', test_data_5)
def test_delete(arg1, arg2, arg3):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.delete_node(arg1, arg2) == arg3
