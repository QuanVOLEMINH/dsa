import pytest


def second_largest(arr):
    if len(arr) == 0:
        return -float('inf')

    first, second = -float('inf'), -float('inf')

    for num in arr:
        if num > first:
            second = first
            first = num
        elif num != first and num > second:
            second = num

    return second


test_data = [
    ([4, 3, 2, 1, 4, 5, 5], 4),
    ([9, 7, 12, 8, 6, 12, 1, 5, 9], 9),
    ([1, 1, 1, 1], -float('inf')),
    ([], -float('inf')),
    ([-1, -2, -4, -5, -3, -2, -4, -5], -2)
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    assert second_largest(arg1) == arg2
