class Solution:
    def permutations1(self, s1: str, s2: str) -> bool:
        """
        method 1: using array and substring of s2 which has the same size as s1
        """
        def get_idx(char: str) -> int:
            return ord(char) - ord('a')

        indices_1 = [0]*26
        for _, c in enumerate(s1):
            indices_1[get_idx(c)] += 1

        # check all substring of s1 that has same size as s2
        for i in range(len(s2)-len(s1)+1):
            indices_2 = [0]*26
            for j in range(len(s1)):
                indices_2[get_idx(s2[i+j])] += 1

            if (indices_1 == indices_2):
                return True

        return False

    def permutations2(self, s1: str, s2: str) -> bool:
        """
        method 2: using sliding window
        """
        def get_idx(char: str) -> int:
            return ord(char) - ord('a')

        indices_1, indices_2 = [0]*26, [0]*26

        for i, c in enumerate(s1):
            indices_1[get_idx(c)] += 1
            indices_2[get_idx(s2[i])] += 1

        for i in range(len(s2)-len(s1)):
            if indices_1 == indices_2:
                return True
            indices_2[get_idx(s2[i])] -= 1
            indices_2[get_idx(s2[i+len(s1)])] += 1

        return False

    def permutations3(self, s1: str, s2: str) -> bool:
        """
        method3: optimized sliding window
        """
        def get_idx(char: str) -> int:
            return ord(char) - ord('a')

        indices_1, indices_2 = [0]*26, [0]*26

        for i, c in enumerate(s1):
            indices_1[get_idx(c)] += 1
            indices_2[get_idx(s2[i])] += 1

        count = 0
        for i in range(26):
            if indices_1[i] == indices_2[i]:
                count += 1

        for i in range(len(s2)-len(s1)):
            print(indices_1)
            print(indices_2)
            print(count)
            print()
            right_idx = get_idx(s2[i+len(s1)])
            left_idx = get_idx(s2[i])

            if count == 26:
                return True

            indices_2[right_idx] += 1
            if indices_2[right_idx] == indices_1[right_idx]:
                count += 1
            elif indices_2[right_idx] == indices_1[right_idx] + 1:
                count -= 1

            indices_2[left_idx] -= 1
            if indices_2[left_idx] == indices_1[left_idx]:
                count += 1
            elif indices_2[left_idx] == indices_1[left_idx] - 1:
                count -= 1

        return count == 26


solution = Solution()

assert solution.permutations1("ab", "eidbaooo") == True
assert solution.permutations1("ab", "eidboaoo") == False

assert solution.permutations2("ab", "eidbaooo") == True
assert solution.permutations2("ab", "eidboaoo") == False

print(solution.permutations3("ab", "eidboaoo"))
