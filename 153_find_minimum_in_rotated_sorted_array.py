"""
    https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/
"""

from typing import List
import pytest


class Solution:
    """
    Solution goes here
    """

    def find_min(self, nums: List[int]) -> int:
        """
        Binary search
        Compare mid value to the rightmost value
        """
        if len(nums) == 0:
            return -1

        left_idx, right_idx = 0, len(nums) - 1
        while left_idx < right_idx:
            mid = left_idx + (right_idx-left_idx)//2

            if nums[mid] < nums[right_idx]:
                right_idx = mid
            else:
                left_idx = mid+1

        return nums[left_idx]


test_data = [
    ([1, 2, 3, 4, 5], 1),
    ([3, 4, 5, 1, 2], 1),
    ([4, 5, 6, 7, 0, 1, 2], 0),
    ([8, 5, 6, 7], 5)
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()
    assert solution.find_min(arg1) == arg2
