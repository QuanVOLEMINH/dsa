class Node:

    # Constructor to initialize
    # the node object
    def __init__(self, data):

        self.data = data
        self.next = None


class LinkedList:

    # Function to initialize head
    def __init__(self):
        self.head = None

    # Recursive function to print
    # linked list in reverse order
    def print_rev(self, temp):
        if not temp:
            return
        self.print_rev(temp.next)
        print(f'{temp.data} ', end='')

    # Function to insert a new node
    # at the beginning

    def push(self, new_data):

        new_node = Node(new_data)
        new_node.next = self.head
        self.head = new_node

    def print_str(self, temp):
        if not temp:
            return
        print(temp.data, end=' ')
        self.print_str(temp.next)


llist = LinkedList()

llist.push(4)
llist.push(3)
llist.push(2)
llist.push(1)

llist.print_str(llist.head)
print()
llist.print_rev(llist.head)
