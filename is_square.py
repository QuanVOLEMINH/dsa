"""

"""

import pytest


class Solution:
    def solve(self, n) -> bool:
        
        if n<=0:return False
        if n==1:return True

        cur_num = 1
        cur_sq = 1
        while cur_sq <=n:
            if cur_sq == n: return True

            cur_num+=1
            cur_sq = cur_num*cur_num
        return False



test_data = [
    (0, False),
    (1, True),
    (4, True),
    (1234*1234, True),
    (1234*5678, False)
]

@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    s = Solution()

    assert s.solve(arg1) == arg2

