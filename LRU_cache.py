class LRUCache:

    def __init__(self, capacity: int):
        if capacity <= 0:
            raise Exception("negative")

        self.capacity = capacity
        self.cache = {}

    def get(self, key: int) -> int:
        if key in self.cache:
            self.put(key, self.cache[key])
        return self.cache.get(key, -1)

    def put(self, key: int, value: int) -> None:
        # if key does not exist => return None, if we omit this arg => exception
        self.cache.pop(key, None)
        self.cache[key] = value
        if len(self.cache) > self.capacity:
            # next(iter()) get the bottom element of keys stack
            del self.cache[next(iter(self.cache))]
