"""
Merge 2 sorted arrays to one sorted array
"""

from typing import List
import pytest


def merge_sorted(arr1: List[int], arr2: List[int]) -> List[int]:
    """
    Run through 2 arrays at a same time
    """
    m = len(arr1)
    n = len(arr2)

    idx1 = idx2 = idx3 = 0
    result = [None] * (m+n)  # space O(m+n)

    # time O(m+n)
    while idx1 < m and idx2 < n:

        temp = 0
        if arr1[idx1] < arr2[idx2]:
            temp = arr1[idx1]
            idx1 += 1
        else:
            temp = arr2[idx2]
            idx2 += 1

        result[idx3] = temp
        idx3 += 1

    # remaining elements in arr1
    while idx1 < m:
        result[idx3] = arr1[idx1]
        idx1 += 1
        idx3 += 1

    # arr 2
    while idx2 < n:
        result[idx3] = arr2[idx2]
        idx2 += 1
        idx3 += 1

    return result


def merge_sorted_reversely(arr1: List[int], arr2: List[int]) -> List[int]:
    """
    Run through 2 arrays at the same time but from end to beginning
    """
    m = len(arr1)
    n = len(arr2)

    # merge from right to left, can do the same from left to right
    idx1 = m - 1
    idx2 = n - 1
    idx3 = m + n - 1

    result = [None] * (m+n)  # space O(m+n)

    # time O(m+n)
    while idx1 >= 0 and idx2 >= 0:
        if arr1[idx1] > arr2[idx2]:
            result[idx3] = arr1[idx1]
            idx1 -= 1
        else:
            result[idx3] = arr2[idx2]
            idx2 -= 1
        idx3 -= 1

    while idx1 >= 0:
        result[idx3] = arr1[idx1]
        idx1 -= 1
        idx3 -= 1

    while idx2 >= 0:
        result[idx3] = arr2[idx2]
        idx2 -= 1
        idx3 -= 1

    return result


test_data = [
    ([1, 3, 4], [2, 5], [1, 2, 3, 4, 5]),
    ([1, 2], [3, 4, 5], [1, 2, 3, 4, 5]),
    ([6], [3, 4, 5], [3, 4, 5, 6]),
    ([-5, 3, 6, 12, 15], [-12, -10, -6, -3, 4, 10],
     [-12, -10, -6, -5, -3, 3, 4, 6, 10, 12, 15])
]


@pytest.mark.parametrize('arr1, arr2, expected_arr', test_data)
def test1(arr1, arr2, expected_arr):
    """
    test goes here
    """
    assert merge_sorted(arr1, arr2) == expected_arr
    assert merge_sorted_reversely(arr1, arr2) == expected_arr


pytest.main()
