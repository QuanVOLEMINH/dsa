"""
My implementation of common data structures
"""
import math


class QTreeNode:
    """
    Represent a node in a binary tree
    """

    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None

    def set_left(self, left):
        """
        set the left node
        """
        self.left = left
        return self

    def set_right(self, right):
        """
        set the right node
        """
        self.right = right
        return self

    def __str__(self):
        return f'{{key: {self.key}, left: {str(self.left)}, right: {str(self.right)}}}'


class QBinaryTree:
    """
    Represent a binary tree
    """

    def __init__(self, root: QTreeNode):
        self.root = root

    def print_root(self):
        """
        print the tree
        """

        queue = [(self.root, 0)]
        node_dict = {}
        while len(queue) > 0:
            last_elem, h_level = queue.pop(0)

            if h_level in node_dict:
                node_dict[h_level].append(last_elem.key)
            else:
                node_dict[h_level] = [last_elem.key]

            if last_elem.left is not None:
                queue.append((last_elem.left, h_level+1))

            if last_elem.right is not None:
                queue.append((last_elem.right, h_level+1))

        for value in node_dict.values():
            print(*value)

    def __str__(self):
        return str(self.root)


class QLNode:
    """
    Represent a node in a linked list
    """

    def __init__(self, val):
        self.val = val
        self.next = None

    def __str__(self):
        return str(self.val)

    def set_next(self, next_node):
        """
        Set the next node of current node
        """
        self.next = next_node
        return self


class QLinkedList:
    """
    Represent a linked list
    """

    def __init__(self, head: QLNode):
        self.head = head

    def __str__(self):
        cur = self.head
        result = ['[']
        while cur is not None:
            result.append(str(cur))
            if cur.next is not None:
                result.append(' -> ')
            cur = cur.next
        result.append(']')
        return ''.join(result)


class QDLNode:
    """
    Represent a node in doubly linked list
    """

    def __init__(self, val):
        self.prev = None
        self.next = None
        self.val = val

    def __str__(self):
        return str(self.val)

    def set_prev(self, prev):
        """
        set the previous node
        """
        cur = prev
        while cur.next is not None:
            cur = cur.next

        self.prev = cur
        cur.next = self
        return self

    def set_next(self, next_node):
        """
        set the next node
        """
        self.next = next_node
        next_node.prev = self
        return self


class QDLinkedList:
    """
    Represent a doubly linked list
    """

    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail

    def __repr__(self):
        result = ['[']
        cur = self.head
        while cur is not None:
            result.append(str(cur.val))
            if cur.next is not None:
                result.append(' <-> ')
            cur = cur.next
        result.append(']')

        return ''.join(result)


class QMaxHeap:
    """
    Represent a max heap.
    Heap is an array visualized as a nearly complete binary tree. Complete when
    number of nodes = 2^k - 1, with k is the number of levels
    """

    def __init__(self, input_arr):
        self.heap = input_arr
        self.size = len(input_arr)
        self.__build_max_heap()

    def extract_max(self):
        """
        Extract the max element in the heap then remove it.
        Correct the remaining tree if necessary to keep it being a max heap.
        Return the max element.
        """

        # max element is alway the root (first item in the array)
        # swap it with the last node in the tree (last item in the array
        self.heap[0], self.heap[self.size -
                                1] = self.heap[self.size-1], self.heap[0]

        # remove last node, this is the expected max element
        result = self.heap.pop()
        self.size -= 1

        # run heapify to make the remaining heap satisfy max heap property
        self.__max_heapify(0)

        return result

    def __build_max_heap(self):
        """
        Produce a max heap from un ordered array.
        """
        for i in range(self.size//2, -1, -1):
            self.__max_heapify(i)

    def __get_child_idx(self, root_idx):
        """
        Get child indexes of root_idx in the array.
        """
        return (root_idx*2 + 1, root_idx*2+2)

    def __max_heapify(self, idx):
        """
        Assume that the trees rooted at left(i) and right(i) are max-heaps.
        This method will correct a single violation of the heap property in a
        subtree at its root.
        """
        size = self.size

        left_idx, right_idx = self.__get_child_idx(idx)

        # get idx of largest element between root, left child and right child
        if left_idx < size and self.heap[left_idx] > self.heap[idx]:
            largest = left_idx
        else:
            largest = idx

        if right_idx < size and self.heap[right_idx] > self.heap[largest]:
            largest = right_idx

        # if the largest element of these 3 is not at root, then exchange it
        # with current idx
        if largest != idx:
            self.heap[largest], self.heap[idx] = self.heap[idx], self.heap[largest]

            # check property at largest after swap
            self.__max_heapify(largest)

    def __str__(self):
        """
        Return str that visualises the array under a tree by levels.
        """
        size = self.size
        level_count = math.ceil(math.log(size, 2))
        result = ''
        for i in range(level_count):
            arr = [str(self.heap[j])
                   for j in range(2**i-1, 2**(i+1)-1) if j < size]
            result += ' '.join(arr)
            if i < level_count - 1:
                result += '\n'
        return result
