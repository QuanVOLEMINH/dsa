import pytest


def top_avg(scores, top=5):

    result = []
    id_count = {}
    temp = 0
    highest_avg = 0
    scores.sort(key=lambda item: (item[0], -item[1]))  # O(nlogn)

    # we can do it only when the scores arr is sorted
    for (s_id, score) in scores:  # O(n)
        if s_id not in id_count:
            id_count[s_id] = 1
            temp += score

        else:
            if id_count[s_id] < top:
                id_count[s_id] += 1
                temp += score

                if id_count[s_id] == top:
                    avg_score = temp // top
                    result.append([s_id, avg_score])
                    if avg_score > highest_avg:
                        highest_avg = avg_score
                    temp = 0

    print(highest_avg)

    return result


def test_top_default():
    scores = [[1, 91], [1, 92], [2, 93], [2, 97], [1, 60], [
        2, 77], [1, 65], [1, 87], [1, 100], [2, 100], [2, 76]]
    assert top_avg(scores) == [[1, 87], [2, 88]]


def test_top_3():
    scores = [[1, 91], [1, 92], [2, 93], [2, 97], [1, 60], [
        2, 77], [1, 65], [1, 87], [1, 100], [2, 100], [2, 76]]
    assert top_avg(scores, 3) == [[1, 94], [2, 96]]


def test_more():
    scores = [[1, 91], [1, 92], [5, 93], [5, 97], [1, 60], [
        5, 77], [1, 65], [1, 87], [1, 100], [5, 100], [5, 76]]
    assert top_avg(scores, 3) == [[1, 94], [5, 96]]


if __name__ == "__main__":
    pytest.main()
