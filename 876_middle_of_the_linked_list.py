"""
https://leetcode.com/problems/middle-of-the-linked-list/
"""

import pytest
from q_data_structures import QLNode, QLinkedList


class Solution:
    """
    Solution goes here
    """

    def solve(self, arg1) -> int:
        """
        Floyd fast and slow pointers
        """
        slow = fast = arg1.head

        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next

        return slow.val


# quality over quantity when it comes to test cases
head_1 = QLNode(1).set_next(QLNode(2).set_next(
    QLNode(3).set_next(QLNode(4).set_next(QLNode(5)))))
linked_list_1 = QLinkedList(head_1)


head_2 = QLNode(1).set_next(QLNode(2).set_next(
    QLNode(3).set_next(QLNode(4).set_next(QLNode(5).set_next(QLNode(6))))))
linked_list_2 = QLinkedList(head_2)


test_data = [
    (linked_list_1, 3),
    (linked_list_2, 4)
]


@ pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.solve(arg1) == arg2
