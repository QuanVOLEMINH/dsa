"""
https://leetcode.com/problems/move-zeroes/
"""

import pytest


class Solution:
    def move_to_end(self, arg1):

        last_non_zero_idx = 0
        for n in arg1:
            if n != 0:
                arg1[last_non_zero_idx] = n
                last_non_zero_idx += 1

        for idx in range(last_non_zero_idx, len(arg1)):
            arg1[idx] = 0

        return arg1

    def move_to_beginning(self, arg1):

        last_non_zero_idx = len(arg1)-1
        for n in arg1[::-1]:
            if n != 0:
                arg1[last_non_zero_idx] = n
                last_non_zero_idx -= 1

        for idx in range(last_non_zero_idx+1):
            arg1[idx] = 0

        return arg1


test_data = [
    ([0, 1, 0, 3, 12], [1, 3, 12, 0, 0]),
    ([0], [0]),
    ([0, 0, 1], [1, 0, 0]),
    ([1, 0, 0], [1, 0, 0])
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    s = Solution()

    assert s.move_to_end(arg1) == arg2


test_data_2 = [
    ([0, 1, 0, 3, 12], [0, 0, 1, 3, 12]),
    ([0], [0]),
    ([0, 0, 1], [0, 0, 1]),
    ([1, 0, 0], [0, 0, 1])
]


@pytest.mark.parametrize('arg1, arg2', test_data_2)
def test2(arg1, arg2):
    s = Solution()

    assert s.move_to_beginning(arg1) == arg2
