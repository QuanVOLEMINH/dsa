"""
https://leetcode.com/problems/n-queens/
"""

import pytest


class Solution:
    def solve(self, n: int) -> [[str]]:
        col = [0] * (n+n)
        diag1 = [0] * (n+n)
        diag2 = [0] * (n+n)
        board = [['.'] * n for i in range(n)]
        result = []

        def search(y) -> None:
            if y == n:
                result.append([''.join(row) for row in board])
                return

            for x in range(n):
                if col[x] or diag1[x+y] or diag2[x-y+n-1]:
                    continue
                col[x] = diag1[x+y] = diag2[x-y+n-1] = 1
                board[x][y] = 'Q'
                search(y+1)
                col[x] = diag1[x+y] = diag2[x-y+n-1] = 0
                board[x][y] = '.'

        search(0)

        print(result)
        return result


test_data = [
    (1, [["Q"]]),
    (4, [[".Q..", "...Q", "Q...", "..Q."], ["..Q.", "Q...", "...Q", ".Q.."]])
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    s = Solution()

    assert sorted(s.solve(arg1)) == sorted(arg2)
