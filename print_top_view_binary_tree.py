"""
Print top view of a binary tree
"""
from q_data_structures import QTreeNode


def print_top_view(root_node: QTreeNode) -> None:
    """
    store vertical order (Horizontal distance) in a dict if it does not exist
    => store only first node at each level
    print dict
    """
    node_dict = dict()
    hor_dis = 0

    queue = [(root_node, hor_dis)]

    while len(queue) > 0:  # O(n), n = nodes count
        cur_node, cur_hd = queue.pop(0)

        if cur_hd not in node_dict:
            node_dict[cur_hd] = cur_node.key

        if cur_node.left:
            queue.append((cur_node.left, cur_hd+1))

        if cur_node.right:
            queue.append((cur_node.right, cur_hd-1))

    for _, value in sorted(node_dict.items(), reverse=True):  # O(klogk)
        print(value)


root = QTreeNode(1)
root.left = QTreeNode(2)
root.right = QTreeNode(3)
root.left.left = QTreeNode(4)
root.left.right = QTreeNode(5)
root.right.left = QTreeNode(6)
root.right.right = QTreeNode(7)
root.right.left.right = QTreeNode(8)
root.right.right.right = QTreeNode(9)

print_top_view(root)
