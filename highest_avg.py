"""
Calculate the highest avg score
"""
from typing import List
import pytest


def find_max_avg(arr: List[tuple]) -> int:
    """
    naive approach
    dict with "name": [scores]
    loop through dict => calculate avg score => find max
    """

    if not arr:
        return 0

    d = {}
    # breakpoint()
    max_avg = 0
    for name, score in arr:  # O(n)
        if name not in d:
            d[name] = [0, 0]

        d[name][0] += int(score)
        d[name][1] += 1

    for (score, count) in d.values():  # O(m), m<n
        avg = score // count
        if avg > max_avg:
            max_avg = avg
    return max_avg


testdata = [
    ([("Bob", "87"), ("Mike", "35"), ("Bob", "52"),
      ("Jason", "35"), ("Mike", "55"), ("Jessica", "99")], 99)
]


@pytest.mark.parametrize("arr, max_avg", testdata)
def test1(arr, max_avg):
    """
    Tests
    """
    assert find_max_avg(arr) == max_avg
