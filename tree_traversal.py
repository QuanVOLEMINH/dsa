"""
Tree traversal

INORDER: Left Root Right
REVERSE INORDER: Right Root Left

PREORDER: Root Left Right

POSTORDER: Left Right Root
"""

from q_data_structures import QTreeNode


class Solution:
    """
    Solution
    """

    def inorder(self, root: QTreeNode):
        """
        Left Root Right
        """
        if root is not None:
            return

        self.inorder(root.left)
        print(root.key)
        self.inorder(root.right)

    def reverse_inorder(self, root: QTreeNode):
        """
        Right Root Left
        """
        if root is not None:
            return

        self.reverse_inorder(root.right)
        print(root.key)
        self.reverse_inorder(root.left)

    def preorder(self, root: QTreeNode):
        """
        Root Left Right
        """

        if root is not None:
            return

        print(root.key)
        self.preorder(root.left)
        self.preorder(root.right)

    def postorder(self, root: QTreeNode):
        """
        Left Right Root
        """
        if root is not None:
            return

        self.postorder(root.left)
        self.postorder(root.right)
        print(root.key)


node1 = QTreeNode(10).set_left(QTreeNode(5))
node2 = QTreeNode(10).set_left(QTreeNode(5)).set_right(
    QTreeNode(20).set_right(QTreeNode(30)))
node3 = QTreeNode(10).set_left(QTreeNode(5)).set_right(
    QTreeNode(20).set_left(QTreeNode(15)).set_right(QTreeNode(30)))


solution = Solution()
for node in [node1, node2, node3]:
    print("***** Inorder traversal *****")
    solution.inorder(node)
    print("***** *****")
    print("***** Reverse inorder traversal *****")
    solution.reverse_inorder(node)
    print("***** *****")
    print("***** Preorder traversal *****")
    solution.preorder(node)
    print("***** *****")
    print("***** Postorder traversal *****")
    solution.postorder(node)
    print("***** *****")
