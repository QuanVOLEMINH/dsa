"""
Next palindrome date from today
"""

from datetime import timedelta
from datetime import date


def is_palindrome(input_str: str) -> bool:
    """
    Check if a string is a palindrome, i.e it could be read the same backward or forward
    """
    if not input_str:
        return False

    left = 0
    right = len(input_str) - 1

    while left < right:
        if input_str[left] != input_str[right]:
            return False
        left += 1
        right -= 1

    return True


def next_palindrome(count=3) -> str:
    """
    Find the n next palindrome dates
    """
    ans = []
    cur_date = date.today()
    one_day_offset = timedelta(days=1)
    i = 0
    while i < count:
        cur_str = cur_date.strftime('%d%m%Y')
        if is_palindrome(cur_str):
            ans.append(cur_str)
            i += 1
        cur_date += one_day_offset

    return ans


print(next_palindrome())
