"""
    https://leetcode.com/problems/combination-sum-iv/
"""
from typing import List


class Solution:
    def combinationSum4(self, nums: List[int], target: int) -> int:

        def num_ways(nums, target, memo):
            if target < 0:
                return 0
            if target == 0:
                return 1

            if memo[target] != None:
                return memo[target]

            ways = 0
            for num in nums:
                ways += num_ways(nums, target-num, memo)

            memo[target] = ways
            return memo[target]

        return num_ways(nums, target, [None]*(target+1))


print(Solution().combinationSum4([1, 2, 3], 4))
