from typing import List
import pytest
from statistics import median

# O(log(n)) as array size is divided by 2 for each iteration


def median_2_sorted_arrays_same_size(arr1: List[int], arr2: List[int]) -> float:

    assert len(arr1) == len(arr2)

    if len(arr1) == 0:
        return 0

    if len(arr1) == 1:
        return (arr1[0] + arr2[0])/2

    if len(arr1) == 2:
        # can do it only when 2 arrays are sorted
        return (max(arr1[0], arr2[0]) + min(arr1[1], arr2[1]))/2

    len_arr = len(arr1)
    m1 = arr1[len_arr//2]  # O(1)
    m2 = arr2[len_arr//2]  # O(1)

    if m1 > m2:
        if len_arr % 2 == 0:
            return median_2_sorted_arrays_same_size(arr1[:len_arr//2+1], arr2[len_arr//2-1:])
        else:
            return median_2_sorted_arrays_same_size(arr1[:len_arr//2+1], arr2[len_arr//2:])

    else:
        if len_arr % 2 == 0:
            return median_2_sorted_arrays_same_size(arr1[len_arr//2-1:], arr2[:len_arr//2+1])
        else:
            return median_2_sorted_arrays_same_size(arr1[len_arr//2:], arr2[:len_arr//2+1])


test_data = [
    ([1, 2, 3, 4, 5], [11, 12, 13, 14, 15]),
    ([1], [2]),
    ([1, 2], [3, 4]),
    ([4, 5, 6, 7], [1, 3, 8, 9])
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test1(arg1, arg2):
    assert median_2_sorted_arrays_same_size(
        arg1, arg2) == median(sorted(arg1+arg2))


pytest.main()
