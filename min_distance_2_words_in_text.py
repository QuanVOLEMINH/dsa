import pytest

# Min max distance between 2 words in a text
# space O(1)
# time O(n) with n = length of input string


def min_distance(s: str, word1: str, word2: str) -> int:
    if word1 == word2:
        return 0

    n = len(s)
    arr = s.split(' ')
    prev = 0
    min_dist = n

    # find first index
    for i, word in enumerate(arr):
        if word == word1 or word == word2:
            prev = i
            break

    for i, word in enumerate(arr):
        if word == word1 or word == word2:
            if word != arr[prev]:
                min_dist = min(i - prev - 1, min_dist)

            prev = i

    return min_dist


test_data = [
    ('geeks for geeks contribute practice', 'geeks', 'practice', 1),
    ('the quick the brown quick brown the frog', 'quick', 'frog', 2)
]


@pytest.mark.parametrize('arg1, arg2, arg3, arg4', test_data)
def test1(arg1, arg2, arg3, arg4):
    assert min_distance(arg1, arg2, arg3) == arg4


pytest.main()
