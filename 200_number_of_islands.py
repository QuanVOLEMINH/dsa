"""
    https://leetcode.com/problems/number-of-islands/
"""

import pytest


class Solution():
    def num_islands(self, grid):
        """
            Idea: loop through grid, if meet 1
                => this is an island, count += 1
                => explore all adjacent point (4 direction) 
                => mark explored point as X
        """
        if not grid:
            return 0
        new_grid = [[col for col in row] for row in grid]  # to not change grid
        count = 0
        for row, _ in enumerate(new_grid):
            for col, _ in enumerate(new_grid[row]):
                if new_grid[row][col] == '1':
                    self.explore(new_grid, row, col)
                    count += 1
        return count

    def explore(self, grid, row, col):
        grid[row][col] = 'X'  # already visited
        # breakpoint()
        # visit neighbors + checking if we're on at edge
        if row-1 >= 0 and grid[row-1][col] == '1':
            self.explore(grid, row - 1, col)
        if row + 1 < len(grid) and grid[row+1][col] == '1':
            self.explore(grid, row+1, col)
        if col-1 >= 0 and grid[row][col-1] == '1':
            self.explore(grid, row, col-1)
        if col + 1 < len(grid[row]) and grid[row][col+1] == '1':
            self.explore(grid, row, col+1)

    def print_grid(self, grid):
        for v in grid:
            print(v)
        print('------')


grid1 = [
    ['1', '0', '0', '1'],
    ['1', '1', '0', '0'],
    ['1', '0', '0', '1']
]

test_data = [
    (grid1, 3)
]


@pytest.mark.parametrize('grid, nb', test_data)
def test(grid, nb):
    assert Solution().num_islands(grid) == nb


pytest.main()
