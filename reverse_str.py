import pytest


def reverse_arr(arr: []) -> []:

    len_arr = len(arr)

    for idx1 in range(len_arr//2):
        idx2 = len_arr - 1 - idx1
        arr[idx1], arr[idx2] = arr[idx2], arr[idx1]

    return arr


def reverse_text(text: str) -> str:
    return ''.join(reverse_arr(list(text)))


def reverse_int(arr: [int]) -> [int]:
    return reverse_arr(list(arr))  # input new list


test_data = [
    ('abcxyz', 'zyxcba'),
    ('abckxyz', 'zyxkcba')
]

test_data_2 = [
    ([1, 2, 3, 4], [4, 3, 2, 1])
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test1(arg1, arg2):
    assert reverse_text(arg1) == arg2


@pytest.mark.parametrize('arg1, arg2', test_data_2)
def test2(arg1, arg2):
    assert reverse_int(arg1) == arg2


pytest.main()
