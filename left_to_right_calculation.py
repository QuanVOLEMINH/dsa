
import pytest
import re


def left_to_right_cal(s: str) -> int:
    class SpecNum():
        def __init__(self, val):
            self.val = val

        def __add__(self, other):
            return SpecNum(self.val + other.val)

        def __sub__(self, other):
            return SpecNum(self.val * other.val)

    expr_str = re.sub(r'(\d+)', r'SpecNum(\1)', s).replace('*', '-')
    print(expr_str)
    num = eval(expr_str)
    return num.val


test_data = [
    ('10+3*4+5*2', 114)
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    assert left_to_right_cal(arg1) == arg2
