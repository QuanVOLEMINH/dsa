from typing import List
import pytest


def score_ways_1(point: int, steps: List[int]) -> int:
    counts = [0] * (point+1)
    counts[0] = 1
    counts[1] = 0

    for i in range(2, point+1):
        for j in steps:
            if j <= i:
                counts[i] += counts[i-j]
    print(counts)
    return counts[point]


def score_ways_without_order(n):

    table = [0 for i in range(n+1)]

    table[0] = 1

    for i in range(3, n+1):
        table[i] += table[i-3]
    for i in range(5, n+1):
        table[i] += table[i-5]
    for i in range(10, n+1):
        table[i] += table[i-10]

    return table[n]


test_data = [
    (10, 2),
    (8, 2),
    (15, 4),
    (13, 5)
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test1(arg1, arg2):
    assert score_ways_1(arg1, (3, 5, 10)) == arg2
