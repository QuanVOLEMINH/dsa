class Solution:
    def min_dist(self, letter1, letter2, input_str) -> int:
        """
            Time O(n), Space O(1)
        """
        prev_idx = 0
        for idx, c in enumerate(input_str):  # O(n)
            if c in [letter1, letter2]:
                prev_idx = idx
                break

        min_dist = len(input_str)
        for idx, c in enumerate(input_str):  # O(n)
            if c in [letter1, letter2] and c != input_str[prev_idx]:
                min_dist = min(min_dist, idx - prev_idx - 1)
                prev_idx = idx

        return min_dist

    def max_dist(self, l1, l2, input_str) -> int:
        n = len(input_str)
        min_idx = [n]*26
        max_idx = [0]*26

        for i, c in enumerate(input_str):
            pos = ord(c)-ord('a')
            if min_idx[pos] == n:
                min_idx[pos] = i
            else:
                max_idx[pos] = i

        return max_idx[ord(l2)-ord('a')] - min_idx[ord(l1)-ord('a')] - 1


print(Solution().min_dist('a', 'c', 'abbccanci'))
print(Solution().max_dist('a', 'c', 'abbccanci'))
