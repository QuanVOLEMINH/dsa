"""
https://leetcode.com/problems/subarray-sum-equals-k/
"""
import pytest


class Solution:
    def solve(self, nums: [int], k: int) -> int:
        count = 0
        cum_sum = 0
        d = {0: 1}
        for n in nums:
            cum_sum += n
            count += d.get(cum_sum-k, 0)
            d[cum_sum] = d.get(cum_sum, 0) + 1
        print(d)
        return count


test_data = [
    ([1, 1, 1], 2, 2),
    ([1, 2, 3], 3, 2),
    ([1, -1, 0], 0, 3)
]


@pytest.mark.parametrize('arg1, arg2, arg3', test_data)
def test(arg1, arg2, arg3):
    s = Solution()

    assert s.solve(arg1, arg2) == arg3
