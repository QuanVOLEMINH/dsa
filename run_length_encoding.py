"""
https://www.geeksforgeeks.org/run-length-encoding/
"""

import pytest


class Solution:
    """
    Solution goes here
    """

    def solve(self, arg1):
        """
        Explain briefly the algorithm
        """
        string_len = len(arg1)
        idx = 0
        ans = ''
        while idx < string_len:
            count = 1
            while idx + 1 < string_len and arg1[idx+1] == arg1[idx]:
                idx += 1
                count += 1
            ans += f'{arg1[idx]}{count}'
            idx += 1
        return ans


# quality over quantity when it comes to test cases
test_data = [
    ('wwwwaaadexxxxxx', 'w4a3d1e1x6'),
    ('a', 'a1'),
    ('abcdee', 'a1b1c1d1e2')
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.solve(arg1) == arg2
