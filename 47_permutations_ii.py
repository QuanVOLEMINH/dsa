"""
    https://leetcode.com/problems/permutations-ii/
"""

from typing import Counter, List
import pytest


class Solution:
    """
    Solution
    """

    def permute_unique(self, nums: List[int]) -> List[List[int]]:
        """
            Use counter to keep track of visited elements
        """
        result = []

        def backtrack(comb, counter):
            if len(comb) == len(nums):
                result.append(list(comb))
                return

            for num in counter:
                if counter[num] > 0:
                    comb.append(num)
                    counter[num] -= 1
                    backtrack(comb, counter)
                    comb.pop()
                    counter[num] += 1
        backtrack([], Counter(nums))
        return result


test_data = [
    ([1, 2, 3], [[1, 2, 3],
                 [1, 3, 2],
                 [2, 1, 3],
                 [2, 3, 1],
                 [3, 1, 2],
                 [3, 2, 1]]),
    ([0, 1], [[0, 1],
              [1, 0]]),
    ([1], [[1]]),
    ([1, 1, 2], [[1, 1, 2],
                 [1, 2, 1],
                 [2, 1, 1]])
]


@pytest.mark.parametrize('arg1,arg2', test_data)
def test(arg1, arg2):
    solution = Solution()
    assert solution.permute_unique(arg1) == arg2
