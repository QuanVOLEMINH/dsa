"""
https://leetcode.com/problems/maximum-subarray/
"""

import pytest
from typing import List

class Solution:
    def  maxSubArray(self, nums: List[int]) -> int:
        result = -float('inf')
        sum_k = 0

        for n in nums:
            sum_k = max(n, sum_k+n)
            result = max(result, sum_k)

        return result




test_data = [
    ([-2,1,-3,4,-1,2,1,-5,4], 6),
    ([1], 1),
    ([5,4,-1,7,8], 23)
]

@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    assert Solution().maxSubArray(arg1) == arg2
