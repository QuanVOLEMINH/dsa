"""
Problem link
"""
from copy import deepcopy
from q_data_structures import QDLNode, QDLinkedList


class Solution:
    """
    Solution goes here
    """

    def print_from_right_to_left(self, arg1: QDLinkedList) -> str:
        """
        Run from the tail until prev is None
        """
        tail = deepcopy(arg1.tail)
        result = ['[']
        while tail is not None:
            result.append(str(tail.val))
            if tail.prev is not None:
                result.append(' -> ')
            tail = tail.prev

        result.append(']')
        return ''.join(result)

    def append_right(self, arg1: QDLinkedList, new_node: QDLNode) -> QDLinkedList:
        """
        Append a new node to the right of db ll
        Note: set tail to this new node !!!
        """
        d_arg1 = deepcopy(arg1)
        head = d_arg1.head
        tail = d_arg1.tail
        if head is None:
            head = new_node
            tail = new_node
        else:
            tail.set_next(new_node)
            tail = new_node

        return QDLinkedList(head, tail)

    def remove_right(self, arg1: QDLinkedList) -> QDLinkedList:
        """
        Note: set tail to tail.prev
        """
        d_arg1 = deepcopy(arg1)
        head = d_arg1.head
        tail = d_arg1.tail
        if head is not None:
            tail.prev.next = None
            tail = tail.prev

        return QDLinkedList(head, tail)

    def append_left(self, arg1: QDLinkedList, new_node: QDLNode) -> QDLinkedList:
        """
        Append a new node to the left of db ll
        Note: set head to this new node !!!
        """
        d_arg1 = deepcopy(arg1)
        head = d_arg1.head
        tail = d_arg1.tail
        if head is None:
            head = new_node
            tail = new_node
        else:
            head.set_prev(new_node)
            head = new_node

        return QDLinkedList(head, tail)

    def remove_left(self, arg1: QDLinkedList) -> QDLinkedList:
        """
        Note: set head to head.prev
        """
        d_arg1 = deepcopy(arg1)
        head = d_arg1.head
        tail = d_arg1.tail
        if head is not None:
            head.next.prev = None
            head = head.next

        return QDLinkedList(head, tail)


# quality over quantity when it comes to test cases
mid = QDLNode(3).set_next(QDLNode(5).set_next(QDLNode(7)))
tail = QDLNode(4).set_prev(mid)
head = QDLNode(2).set_next(mid)
db_ll = QDLinkedList(head, tail)

solution = Solution()
print(str(db_ll))
print(solution.print_from_right_to_left(db_ll))

new_dl = solution.append_right(db_ll, QDLNode(10))
print(new_dl)

al_dl = solution.append_left(db_ll, QDLNode(20))
print(al_dl)

rr_dl = solution.remove_right(db_ll)
print(rr_dl)

rl_dl = solution.remove_left(db_ll)
print(rl_dl)
