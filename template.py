"""
Problem link
"""

import pytest


class Solution:
    """
    Solution goes here
    """

    def solve(self, arg1):
        """
        Explain briefly the algorithm
        """
        return arg1


# quality over quantity when it comes to test cases
test_data = [
    ([], []),
    ([], [])
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.solve(arg1) == arg2
