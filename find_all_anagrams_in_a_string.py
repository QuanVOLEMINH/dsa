"""
https://leetcode.com/problems/find-all-anagrams-in-a-string/
"""

from typing import List


class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        def get_idx(char: str) -> int:
            assert len(char) == 1
            return ord(char) - ord('a')

        lp, ls = len(p), len(s)
        if lp > ls:
            return []

        count_p = [0] * 26
        count_s = [0] * 26

        for i, c in enumerate(p):  # O(lp)
            count_p[get_idx(c)] += 1
            count_s[get_idx(s[i])] += 1

        result = []
        for i in range(ls):  # O(ls)
            if count_p == count_s:  # O(1), 26 elements
                result.append(i)

            count_s[get_idx(s[i])] -= 1

            if i + lp < ls:
                count_s[get_idx(s[i+lp])] += 1

        return result


solution = Solution()

print(solution.findAnagrams("cbaebabacd", "abc"))
print(solution.findAnagrams("abab", "ab"))
print(solution.findAnagrams("aaa", "aaaa"))
print(solution.findAnagrams("aaaaaaaaaaa", "aaaaaaaaaa"))
