class Solution:
    def to_base_x(self, num: int, base: int) -> str:
        assert base not in [2, 16]

        if num < 0:
            num = 2**32 - abs(num)  # two's complement

        quotient = num // base
        remainders = [num % base]

        while quotient != 0:
            remainders.insert(0, quotient % base)
            quotient = quotient // base

        return ''.join(str(i) for i in remainders)

    def to_base_10(self, num: int, current_base: int) -> str:

        i = 0
        result = 0
        while num > 0:
            digit = num % 10
            result += digit * (current_base**i)
            num = num // 10
            i += 1

        return result


print(Solution().to_base_x(7, 7))

print(Solution().to_base_x(13346, 7))
print(Solution().to_base_10(53624, 7))

print(Solution().to_base_x(13346, 8))
print(Solution().to_base_10(32042, 8))
