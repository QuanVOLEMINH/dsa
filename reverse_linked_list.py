class ListNode:
    def __init__(self, data=0, next=None):
        self.data = data
        self.next = next


class Solution:
    def print_ll(self, head: ListNode) -> None:
        if not head:
            return
        temp = head
        while temp:
            print(temp.data, end=' ')
            temp = temp.next

    def reverse_ll(self, head: ListNode) -> ListNode:

        if not head:
            return head

        p1 = head  # run pointer
        while p1.next != None:
            p2 = p1.next  # temp pointer
            p1.next = p2.next
            p2.next = head
            head = p2

        return head


node4 = ListNode(4, None)
node3 = ListNode(3, node4)
node2 = ListNode(2, node3)
head = ListNode(1, node2)

solution = Solution()
solution.print_ll(head)
print("\n=====")
solution.print_ll(solution.reverse_ll(head))
