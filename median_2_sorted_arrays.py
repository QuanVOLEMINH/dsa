from typing import List


class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:

        # x x [x] | [x] x x
        #   y [y] | [y] y
        # x x y [y] [x] | [y] y [x] x x
        # return l_short, r_short, l_long, r_long

        def get_val(arr, i):
            if i == -1:
                return float('-inf')
            if i == len(arr):
                return float('inf')
            return arr[i]

        def get_indices(r_short, a_short, a_long):
            mid_idx = (len(a_short)+len(a_long))//2  # half number of items
            r_long = mid_idx - r_short

            return (r_short-1, r_short, r_long-1, r_long)

        def get_direction(l_short, r_short, l_long, r_long, a_short, a_long):
            if get_val(a_short, l_short) > get_val(a_long, r_long):
                return -1
            elif get_val(a_long, l_long) > get_val(a_short, r_short):
                return 1
            return 0

        def get_result(l_short, r_short, l_long, r_long, a_short, a_long):
            odd = (len(a_short)+len(a_long)) % 2

            if odd:  # one more element on the right than the left
                return min(get_val(a_short, r_short), get_val(a_long, r_long))
            else:
                return (max(get_val(a_short, l_short), get_val(a_long, l_long)) + min(get_val(a_short, r_short), get_val(a_long, r_long))) / 2

        # get shorter/longer array
        a_short, a_long = nums1, nums2

        if len(nums1) > len(nums2):
            a_short, a_long = nums2, nums1

        # binary search on the smaller array
        l = 0
        r = len(a_short)
        l_short = r_short = l_long = r_long = d = 1
        while d != 0:
            m = (l+r)//2

            l_short, r_short, l_long, r_long = get_indices(m, a_short, a_long)

            # determine direction
            d = get_direction(l_short, r_short, l_long,
                              r_long, a_short, a_long)
            if d < 0:
                r = m-1
            else:
                l = m+1
        print(l_short, r_short, l_long, r_long)
        return get_result(l_short, r_short, l_long, r_long, a_short, a_long)
