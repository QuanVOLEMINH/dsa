

def sum_simplify(frac1: str, frac2: str) -> str:
    def gcd(a: int, b: int) -> int:
        """
            gcd calculation based on Euler's algorithm
        """
        while b != 0:
            a, b = b, a % b
        return a

    n1, d1 = map(int, frac1.strip().split('/'))
    n2, d2 = map(int, frac2.strip().split('/'))

    n = n1*d2 + n2*d1
    d = d1*d2
    g = gcd(n, d)

    return f'{n//g}/{d//g}'


print(sum_simplify('1/2', '1/4'))
