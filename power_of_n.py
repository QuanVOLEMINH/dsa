"""
Power of n
"""


class Solution:
    """
    Solution goes here
    """

    def is_power_of_n(self, n: int, p: int) -> bool:
        """
        Algorithm
        """
        # normal approach
        if n == 0:
            return False

        while n % p == 0:
            n /= p

        return n == 1

        # as n in range [-2^31+1, 2^31 + 1] => 32-bit integer
        # the highest power of 3 that n can hold is 3^19
        # if n is a power of 3 then n divides 3^19
        # precompute 3^19 = 1162261467
        #
        # return n > 0 and 1162261467 % n == 0


print(Solution().is_power_of_n(27, 3))
print(Solution().is_power_of_n(1000, 10))
