from collections import defaultdict
from multiprocessing.context import assert_spawning
import pytest


class UndergroundSystem:

    def __init__(self):
        self.check_in = {}
        self.time = defaultdict(lambda: [0, 0])

    def checkIn(self, id: int, stationName: str, t: int) -> None:
        if id in self.check_in:
            raise Exception("already checked in")

        self.check_in[id] = (stationName, t)

    def checkOut(self, id: int, stationName: str, t: int) -> None:
        if id not in self.check_in:
            raise Exception('not checked in')
        check_in_station, check_in_time = self.check_in.pop(id)

        total_with_count = self.time[(check_in_station, stationName)]
        total_with_count[0] += t - check_in_time
        total_with_count[1] += 1

    def getAverageTime(self, startStation: str, endStation: str) -> float:

        total, count = self.time[(startStation, endStation)]

        return total/count


def test():
    obj = UndergroundSystem()
    obj.checkIn(1, "A", 5)
    obj.checkIn(2, "A", 6)
    obj.checkOut(2, "B", 10)
    obj.checkOut(1, "B", 15)
    avg_time = obj.getAverageTime("A", "B")
    assert avg_time == 7
