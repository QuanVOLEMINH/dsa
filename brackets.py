import pytest

pairs = {
    '[': ']',
    '{': '}',
    '(': ')'
}

openers = set(pairs.keys())
closers = set(pairs.values())


def is_validated(s: str) -> bool:

    # s = s.strip().replace(r'\s+', '')

    if len(s) == 0:
        return True

    stack = []

    for c in s:
        if c in openers:
            stack.append(c)
            continue

        if c in closers:
            if len(stack) == 0:
                return False

            head = stack.pop()

            if pairs[head] != c:
                return False

    return len(stack) == 0


test_data = [
    ('{ [] ( ) }', True),
    ('{ [(] ) }', False),
    (' [ }', False),
    ('   }', False)
]


@pytest.mark.parametrize('s, result', test_data)
def test(s: str, result: bool) -> None:
    assert is_validated(s) == result


pytest.main()
