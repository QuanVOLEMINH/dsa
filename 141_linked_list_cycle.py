"""
https://leetcode.com/problems/linked-list-cycle/
"""

from copy import deepcopy
import pytest
from q_data_structures import QLNode, QLinkedList


class Solution:
    """
    Solution goes here
    """

    def find_loop_with_hash_map(self, arg1) -> bool:
        """
        Store visited node's hash code in a dict/set
        """
        hash_nodes = set()
        cur_node = deepcopy(arg1.head)

        while cur_node is not None:
            if cur_node not in hash_nodes:
                hash_nodes.add(cur_node)
            else:
                return True

            cur_node = cur_node.next

        return False

    def find_loop_with_two_ptr(self, arg1) -> bool:
        """
        Floyd's cycle-finding algorithm/slow and fast pointers
        The idea is to have 2 pointers, one pointer is moved by one and another
        is moved by two for each iteration.
        If these pointers meet then there is a loop, when the linked list is
        completely traversed but they do not meet then there is no loop

        How it works: the first distance between these pointer is 2, then 3,then 4
        (increase 1 by each iteration: k+1), when this distance becomes n, they meet
        because these pointers are moving in a cycle of length n.
        """
        cur_node = deepcopy(arg1.head)
        slow_ptr = cur_node
        fast_ptr = cur_node

        while fast_ptr is not None and fast_ptr.next is not None:
            slow_ptr = slow_ptr.next
            fast_ptr = fast_ptr.next.next

            if slow_ptr == fast_ptr:
                return True

        return False


# quality over quantity when it comes to test cases
tail_1 = QLNode(4)
loop_1 = QLNode(2).set_next(QLNode(0).set_next(tail_1))
tail_1.set_next(loop_1)
head_1 = QLNode(3).set_next(loop_1)
linked_list_1 = QLinkedList(head_1)

tail_2 = QLNode(2)
loop_2 = QLNode(1).set_next(tail_2)
tail_2.set_next(loop_2)
head_2 = loop_2
linked_list_2 = QLinkedList(head_2)

head_3 = QLNode(1)
linked_list_3 = QLinkedList(head_3)

test_data = [
    (linked_list_1, True),
    (linked_list_2, True),
    (linked_list_3, False)
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Tests go here
    """
    solution = Solution()

    assert solution.find_loop_with_hash_map(arg1) == arg2
    assert solution.find_loop_with_two_ptr(arg1) == arg2
