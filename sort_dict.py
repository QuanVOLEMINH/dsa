d = {1: 11222, 3: 3423, 4: 12321, 2: 112}


sort_by_key = {key: value for (key, value) in sorted(
    d.items(), key=lambda item: item[0])}

sort_by_value = {key: value for (key, value) in sorted(
    d.items(), key=lambda item: item[1])}

print(sort_by_key)
print(sort_by_value)
