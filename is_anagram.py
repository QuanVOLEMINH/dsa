import pytest
from collections import Counter


def is_anagram1(s1: str, s2: str) -> bool:
    # or using 2 dicts
    return Counter(s1) == Counter(s2)


# space O(1)
# time O(max(m,n))
def is_anagram2(s1: str, s2: str) -> bool:
    count = [0]*256  # O(1)

    for c in s1:  # O(m)
        count[ord(c)] += 1

    for c in s2:  # O(n)
        count[ord(c)] -= 1

    return all(x == 0 for x in count)  # O(1)


test_data = [
    ('ab', 'b', False),
    ('anagram', 'aaagnrm', True)
]


@pytest.mark.parametrize('arg1, arg2, arg3', test_data)
def test1(arg1, arg2, arg3):
    assert is_anagram1(arg1, arg2) == arg3


@pytest.mark.parametrize('arg1, arg2, arg3', test_data)
def test2(arg1, arg2, arg3):
    assert is_anagram2(arg1, arg2) == arg3


pytest.main()
