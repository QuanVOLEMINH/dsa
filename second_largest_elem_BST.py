"""
Second largest element in a BST
"""

import pytest
from q_data_structures import QTreeNode


class Solution:
    """
    Solution
    """

    def solve(self, arg1: QTreeNode) -> int:
        """
        Find the second largest element
        It's the second element when we process a BST using reverse_inorder
        (right, root, left)
        """
        def reverse_inorder(root: QTreeNode, count):
            if root == None:
                return
            reverse_inorder(root.right, count)

            count[0] += 1
            if count[0] == 2:
                count.append(root.key)
                return
            reverse_inorder(root.left, count)

        count = [0]
        reverse_inorder(arg1, count)
        return count[1]


node1 = QTreeNode(10).set_left(QTreeNode(5))
node2 = QTreeNode(10).set_left(QTreeNode(5)).set_right(
    QTreeNode(20).set_right(QTreeNode(30)))
node3 = QTreeNode(10).set_left(QTreeNode(5)).set_right(
    QTreeNode(20).set_left(QTreeNode(15)).set_right(QTreeNode(30)))
test_data = [
    (node1, 5),
    (node2, 20),
    (node3, 20)
]


@ pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    """
    Test
    """
    s = Solution()

    assert s.solve(arg1) == arg2
