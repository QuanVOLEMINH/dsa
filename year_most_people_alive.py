import pytest
from typing import List


def most_alive(years) -> int:

    counts = [0]*(2000-1900+1)  # O(101)
    for (birth, death) in years:  # O(n)
        for i in range(birth, death+1):  # O(max(death-birth))
            counts[i-1900] += 1

    max_count = max(counts)  # O(101)

    # O(101)
    return [1900 + i for i, count in enumerate(counts) if count == max_count]


test_data = [
    ([(1920, 1939), (1911, 1944),
      (1920, 1955), (1938, 1939)], [1938, 1939]),
    ([(1920, 1939), (1911, 1944),
      (1920, 1955), (1938, 1939), (1937, 1940)], [1938, 1939])
]


@pytest.mark.parametrize('arg1, arg2', test_data)
def test(arg1, arg2):
    assert set(most_alive(arg1)) == set(arg2)
