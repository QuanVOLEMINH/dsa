from collections import Counter
from typing import List


def permutations(s: str) -> List[str]:
    if not s:
        return []
    if len(s) == 1:
        return [s]

    result = []

    def permute(s: List[str], start, end):
        if start == end:
            result.append(''.join(s))
        else:
            for idx in range(start, end + 1):
                s[start], s[idx] = s[idx], s[start]  # swap
                permute(s, start + 1, end)
                s[start], s[idx] = s[idx], s[start]  # revert
    permute(list(s), 0, len(s) - 1)
    return result


def permutations2(s: str) -> List[str]:
    """
        ~~ implementation in CPH
    """
    n = len(s)
    result = []

    def search(permutation, chosen):
        if len(permutation) == n:
            result.append(''.join(permutation))
        else:
            for i in range(n):
                if chosen[i]:
                    continue
                chosen[i] = True
                permutation.append(s[i])
                search(permutation, chosen)
                chosen[i] = False
                permutation.pop()
    search([], [False] * n)
    return result


def permutations_unique(s: str) -> List[str]:
    """
        Input has duplicate chars => return only unique permutations
        Keep the occurence of a char by Counter
    """
    n = len(s)
    result = []

    def search(permutation, counter):
        if len(permutation) == n:
            result.append(''.join(permutation))
            return

        for char in counter:
            if counter[char] > 0:
                counter[char] -= 1
                permutation.append(char)
                search(permutation, counter)
                counter[char] += 1
                permutation.pop()

    search([], Counter(s))

    return result


def test_permutation():
    assert Counter(permutations('ABC')) == Counter(
        ['ABC', 'ACB', 'BAC', 'BCA', 'CBA', 'CAB'])
    assert Counter(permutations2('ABC')) == Counter(permutations('ABC'))


def test_permutation_unique():
    assert Counter(permutations_unique('AAB')) == Counter(
        ['AAB', 'ABA', 'BAA'])
    assert Counter(permutations_unique('ABC')) == Counter(permutations2('ABC'))
