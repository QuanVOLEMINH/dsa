import pytest


def k_non_repeating(s: str, k=1) -> str:

    if len(s) == 0:
        return ''

    if len(s) == 1:
        return s[0]

    NB_CHARS = 256  # ascii

    count = [0] * NB_CHARS
    idx = [NB_CHARS] * NB_CHARS

    for i, c in enumerate(s):
        pos = ord(c)

        count[pos] += 1

        if count[pos] == 1:
            idx[pos] = i
        else:  # more than one
            idx[pos] = NB_CHARS  # set an invalid idx

    idx.sort()  # O(1) as idx has only 256 items
    # breakpoint()

    return s[idx[k-1]] if idx[k-1] < NB_CHARS else ''


def k_non_repeating_opt(s: str, k=1) -> str:
    """
    1. Make an array of 256 ascii letters and set init value to an invalid index
    (n)
    2.If meet once => value == invalid_idx => set this position to index of
    character in the string. If meet more than once => value < invalid_idx => set value = invalid_idx + 1
    3. Sort this array
    4. Take the character at the array[k-1] position of the string.
    """
    if len(s) == 0:
        return ''

    if len(s) == 1:
        return s[0]

    NB_CHARS = 256  # ascii

    idx = [NB_CHARS] * NB_CHARS

    for i, c in enumerate(s):
        pos = ord(c)

        if idx[pos] == NB_CHARS:
            idx[pos] = i
        else:
            idx[pos] = NB_CHARS + 1  # set an invalid idx

    idx.sort()  # O(1) as idx has only 256 items
    # breakpoint()

    return s[idx[k-1]] if idx[k-1] != NB_CHARS else ''


testdata = [
    (1, 'hello', 'h'),
    (1, 'aabbcddd', 'c')
]

testdata2 = [
    (2, 'hello', 'e'),
    (2, 'aabbcddd', '')
]


@pytest.mark.parametrize("k, s, expected_char", testdata)
def test_first_non_repeating(k, s, expected_char):
    assert k_non_repeating(s, k) == expected_char
    assert k_non_repeating_opt(s, k) == expected_char


@pytest.mark.parametrize("k, s, expected_char", testdata2)
def test_second_non_repeating(k, s, expected_char):
    assert k_non_repeating(s, k) == expected_char
    assert k_non_repeating_opt(s, k) == expected_char


pytest.main()
