class Solution:
    def lastSubstring(self, s: str) -> str:
        n = len(s)
        mmax = max(s)  # max letter, O(n)

        candidates = [i for i, c in enumerate(s) if c == mmax]  # O(n)
        offset = 1

        print(candidates)
        while len(candidates) > 1:
            # next max char
            cur_max = max(s[i + offset] for i in candidates if i + offset < n)

            new_candidates = []
            for i, pos in enumerate(candidates):
                # ignore duplication
                if i > 0 and candidates[i-1] + offset == pos:
                    continue

                # find pos where char == cur_max
                if pos + offset < n and s[pos + offset] == cur_max:
                    new_candidates.append(pos)

            print(cur_max, candidates, new_candidates, offset)
            candidates = new_candidates
            offset += 1

        return s[candidates[0]:]


print(Solution().lastSubstring('leetcode'))
print(Solution().lastSubstring('babababa'))
